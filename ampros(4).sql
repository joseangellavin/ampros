-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-12-2019 a las 10:02:44
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ampros`
--
CREATE DATABASE IF NOT EXISTS `ampros` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci;
USE `ampros`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

DROP TABLE IF EXISTS `tarea`;
CREATE TABLE `tarea` (
  `id` int(5) NOT NULL,
  `nombre` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `imagen` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_spanish_ci NOT NULL,
  `locucion` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`id`, `nombre`, `imagen`, `descripcion`, `locucion`) VALUES
(9, 'Cenar', 'cenar.png', 'Cenar', 'cenar.mp3'),
(5, 'lavar_dientes', 'cepillar los dientes.png', 'Lavarse los dientes', 'dientes.mp3'),
(4, 'desayunar', 'desayunar.png', 'Desayuno', 'desayuno.mp3'),
(6, 'ducharse', 'duchar.png', 'Ducharse ', 'ducha.mp3'),
(3, 'hacer_cama', 'hacer la cama.png', 'Hacer la cama', 'hacerCama.mp3'),
(1, 'levantarse', 'levantar.png', 'Levantarse por la mañana', 'cama.mp3'),
(7, 'peinar', 'peinar.png', 'Peinarse', 'peinar.mp3'),
(2, 'vestir', 'vestir.png', 'Vestirse', 'vestir.mp3'),
(8, 'comer', 'comer.png', 'Comida', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_completa`
--

DROP TABLE IF EXISTS `tarea_completa`;
CREATE TABLE `tarea_completa` (
  `id` int(10) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `id_tarea` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tarea_completa`
--

INSERT INTO `tarea_completa` (`id`, `id_usuario`, `fecha`, `hora`, `id_tarea`) VALUES
(57, 2, '2019-12-18', '13:34:53', 1),
(58, 2, '2019-12-18', '13:34:53', 2),
(59, 2, '2019-12-18', '13:34:54', 3),
(60, 2, '2019-12-18', '13:34:55', 4),
(61, 2, '2019-12-18', '13:34:56', 5),
(62, 2, '2019-12-18', '13:34:57', 6),
(63, 2, '2019-12-18', '13:34:58', 7),
(64, 2, '2019-12-18', '13:34:58', 8),
(65, 1, '2019-12-18', '13:35:21', 1),
(66, 1, '2019-12-18', '13:35:21', 2),
(67, 2, '2019-12-19', '09:26:16', 1),
(68, 2, '2019-12-19', '09:26:16', 2),
(69, 2, '2019-12-19', '09:26:17', 3),
(70, 2, '2019-12-19', '09:26:18', 4),
(71, 2, '2019-12-19', '09:26:19', 5),
(72, 2, '2019-12-19', '09:26:19', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tutor`
--

DROP TABLE IF EXISTS `tutor`;
CREATE TABLE `tutor` (
  `id` int(5) NOT NULL,
  `usuario` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `contrasenia` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombre` varchar(25) COLLATE utf8mb4_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `tutor`
--

INSERT INTO `tutor` (`id`, `usuario`, `contrasenia`, `nombre`) VALUES
(1, 'paco', '311020666a5776c57d265ace682dc46d', 'Paco'),
(2, 'jose', '662eaa47199461d01a623884080934ab', 'Jose Angel');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(25) COLLATE utf8mb4_spanish_ci NOT NULL,
  `tutor` int(11) NOT NULL,
  `imagen_login` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_spanish_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `tutor`, `imagen_login`, `nombre`, `apellidos`) VALUES
(1, 'usuario1', 1, 'usuario.jpg', 'Juan', 'Perez'),
(2, 'usuario2', 1, 'usuario.jpg', 'Paco', 'Sanchez');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tarea_completa`
--
ALTER TABLE `tarea_completa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tarea_usuario` (`id_usuario`),
  ADD KEY `id_tarea` (`id_tarea`);

--
-- Indices de la tabla `tutor`
--
ALTER TABLE `tutor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `i_usuario` (`usuario`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `i_usuario_usuario` (`usuario`),
  ADD KEY `usuario_tutor` (`tutor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tarea`
--
ALTER TABLE `tarea`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tarea_completa`
--
ALTER TABLE `tarea_completa`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `tutor`
--
ALTER TABLE `tutor`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tarea_completa`
--
ALTER TABLE `tarea_completa`
  ADD CONSTRAINT `tarea_completa_ibfk_1` FOREIGN KEY (`id_tarea`) REFERENCES `tarea` (`id`),
  ADD CONSTRAINT `tarea_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_tutor` FOREIGN KEY (`tutor`) REFERENCES `tutor` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
