<?php

function getConnection()
{

    $servidor = "localhost";

    $usuario = "root";

    $password = "";

    $bbdd = "ampros";

    try {
        @$conexion = new mysqli($servidor, $usuario, $password, $bbdd);
        if ($conexion->connect_errno) {
            throw new Exception('Fallo en la conexion con la BD: ');
        } else {
            mysqli_set_charset($conexion, 'utf8');
            //echo "Conexion correcta!";
            return $conexion;
        };
    } catch (Exception $e) {
        echo $e->getMessage() . "<br/>" . $conexion->connect_error;
    }
}
