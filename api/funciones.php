<?php
require "db.php";


function alumnos_tutor($request){
    $sql = "SELECT usuario.id,usuario.nombre,usuario.tutor,usuario.imagen_login,usuario.apellidos FROM usuario JOIN tutor ON tutor.id=usuario.tutor WHERE tutor.id=?";
    $id_tutor = $request->getAttribute("tutor");

    try {
        $conexion = getConnection();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("s",$id_tutor);
        if (!$sentencia->execute()) {
            // echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
            
        } else {
            $resultado = $sentencia->get_result();
            $datos = $resultado->fetch_all(MYSQLI_ASSOC);
        }

        $sentencia->close();
        $conexion->close();
        return json_encode($datos);

    
    } catch (Exception $e) {
        //   echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}


function crear_tarea($request)
{
    $body = file_get_contents("php://input");
    // $body = $request->getBody();
    $datos = json_decode($body, true);
   // print_r($datos);
    //print_r($tarea);
    $sql = "INSERT INTO tarea (nombre,imagen,descripcion,locucion) VALUES (?, ?, ?,?)";
    try {
        $conexion = getConnection();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("ssss", $datos["nombre"], $datos["imagen"], $datos["descripcion"], $datos["locucion"]);
        if (!$sentencia->execute()) {
            // echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
            $exito = json_encode(array("exito" => false));
        } else {
            $exito = json_encode(array("exito" => true));
        }

        $sentencia->close();
        $conexion->close();
        return $exito;
    } catch (Exception $e) {

        //  echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}



function crear_usuario($request)
{
    $body = file_get_contents("php://input");
    // $body = $request->getBody();
    $datos = json_decode($body, true);

    //print_r($tarea);
    $sql = "INSERT INTO usuario (usuario,tutor,imagen_login,nombre,apellidos) VALUES (?, ?, ?, ?,?)";
    try {
        $conexion = getConnection();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("sssss", $datos["usuario"], $datos["tutor"], $datos["imagen_login"], $datos["nombre"], $datos["apellidos"]);
        if (!$sentencia->execute()) {
             echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
            $exito = json_encode(array("exito" => false));
        } else {
            $exito = json_encode(array("exito" => true));
        }

        $sentencia->close();
        $conexion->close();
        return $exito;
    } catch (Exception $e) {

        //  echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}




function obtener_datos($request)
{
    $id_usuario = $request->getAttribute("id");


    $sql = "SELECT * FROM usuario WHERE id=?";

    try {
        $datos = null;
        $conexion = getConnection();

        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("s", $id_usuario);
        if (!$sentencia->execute()) {
            //  echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
        } else {
            $resultado = $sentencia->get_result();
            $datos = $resultado->fetch_all(MYSQLI_ASSOC);
        }


        $sentencia->close();
        $conexion->close();
        return json_encode($datos);
    } catch (Exception $e) {
        //  echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}


function tareas_restante_dia($request)
{
    $id_usuario = $request->getAttribute("usuario");
    $fecha = $request->getAttribute("dia");

    $sql = "SELECT * FROM tarea";
    $sql2 = "SELECT * FROM tarea_completa WHERE id_usuario=? AND fecha LIKE ?";
    try {
        $conexion = getConnection();
        $sentencia = $conexion->query($sql);
        $todas = $sentencia->fetch_all(MYSQLI_ASSOC);
        $sentencia->close();

        $sentencia = $conexion->prepare($sql2);
        $sentencia->bind_param("is", $id_usuario, $fecha);
        if (!$sentencia->execute()) {
            //   echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
        } else {
            $resultado = $sentencia->get_result();
            $tareas_completas = $resultado->fetch_all(MYSQLI_ASSOC);
        }
        $tareas = array();
        foreach ($todas as $t) {
            $repetida = false;
            foreach ($tareas_completas as $tc) {
                if ($t["id"] == $tc["id_tarea"])
                    $repetida = true;
            }
            if (!$repetida) {
                array_push($tareas, $t);
            }
        }

        $sentencia->close();
        $conexion->close();
        return json_encode($tareas);
    } catch (Exception $e) {
        //  echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}




function mostrar_login($request)
{


    $sql = "SELECT * FROM usuario";

    try {
        $conexion = getConnection();
        $sentencia = $conexion->query($sql);
        $tareas = $sentencia->fetch_all(MYSQLI_ASSOC);
        $conexion->close();

        return json_encode($tareas);
    } catch (Exception $e) {
        //   echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

function tareas_todas($response)
{

    $sql = "SELECT * FROM tarea;";

    try {
        $conexion = getConnection();
        $sentencia = $conexion->query($sql);
        $tareas = $sentencia->fetch_all(MYSQLI_ASSOC);
        $conexion->close();

        return json_encode($tareas);
    } catch (Exception $e) {
        //  echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}
function tareas_usuario($request)
{
    $fecha = date("Y-m-d");
    $id_usuario = $request->getAttribute("usuario");

    $sql = "SELECT * FROM tarea";
    $sql2 = "SELECT * FROM tarea_completa WHERE id_usuario=? AND fecha LIKE ?";
    try {
        $conexion = getConnection();
        $sentencia = $conexion->query($sql);
        $todas = $sentencia->fetch_all(MYSQLI_ASSOC);
        $sentencia->close();

        $sentencia = $conexion->prepare($sql2);
        $sentencia->bind_param("is", $id_usuario, $fecha);
        if (!$sentencia->execute()) {
            //   echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
        } else {
            $resultado = $sentencia->get_result();
            $tareas_completas = $resultado->fetch_all(MYSQLI_ASSOC);
        }
        $tareas = array();
        foreach ($todas as $t) {
            $repetida = false;
            foreach ($tareas_completas as $tc) {
                if ($t["id"] == $tc["id_tarea"])
                    $repetida = true;
            }
            if (!$repetida) {
                array_push($tareas, $t);
            }
        }
    
        $sentencia->close();
        $conexion->close();
        return json_encode($tareas);
    } catch (Exception $e) {
        //echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}



function completar_tarea($response)
{
    $body = file_get_contents("php://input");
    // $body = $request->getBody();
    $tarea = json_decode($body, true);
    //print_r($tarea);
    $sql = "INSERT INTO tarea_completa (id_usuario,fecha,hora,id_tarea) VALUES (?, ?, ?, ?)";
    try {
        $conexion = getConnection();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("ssss", $tarea["id_usuario"], $tarea["fecha"], $tarea["hora"], $tarea["id_tarea"]);
        if (!$sentencia->execute()) {
            // echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
            return json_encode(array("exito" => false));
        } else {
            return json_encode(array("exito" => true));
        }

        $sentencia->close();
        $conexion->close();
    } catch (Exception $e) {

        //echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}
function login_tutor($request){
    $body = file_get_contents("php://input");
    // $body = $request->getBody();
    $datos = json_decode($body, true);
    $sql = "SELECT * FROM tutor WHERE usuario LIKE ? AND contrasenia LIKE '" . md5($datos['contrasenia']) . "';";

    try {
        $conexion = getConnection();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("s", $datos["usuario"]);
        if (!$sentencia->execute()) {
          //  echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
        } else {
            $resultado = $sentencia->get_result();
            $arrayResultados = $resultado->fetch_all(MYSQLI_ASSOC);
            if (sizeof($arrayResultados) == 0) {
                //echo '  <p class="mt-5 mb-3">Usuario o contraseña incorrectos!</p>';
                $exito = json_encode(array("exito" => false));

            } else {
                $exito = json_encode(array("exito" => true));

            }
           
        }
        $sentencia->close();
        $conexion->close();
        return $exito;
    } catch (Exception $e) {

        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

