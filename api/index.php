<?php


require "vendor/autoload.php";
require "funciones.php";

$api = new Slim\App();


//AQUI SE CREAN LAS RUTAS DE NUESTRA REST API

$api->get("/tareas/todas", "tareas_todas"); 
$api->get("/tareas/{usuario}", "tareas_usuario"); 
$api->get("/tareas/{usuario}/{dia}", "tareas_restante_dia"); 
$api->get("/login/imagenes", "mostrar_login");
$api->get("/usuarios/{tutor}", "alumnos_tutor");

$api->get("/usuario/{id}", "obtener_datos"); 



$api->post("/tarea/completar", "completar_tarea");
$api->post("/usuario/crear", "crear_usuario");
$api->post("/tarea/crear", "crear_tarea");
$api->post("/login/tutor", "login_tutor");

/*
$app->post("/centros/add", "add_Game"); //Agregar un juego

$app->put("/centros/{id}", "update_Game"); //Actualizar un juego

$app->delete("/centros/{id}", "delete_Game"); //Eliminar un juego
*/


$api->run();

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header("Content-Type: application/json; charset=utf-8");
?>
