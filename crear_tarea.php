
<?php 

session_start();
if (!isset($_SESSION["usuario"]) || !($_SESSION["logueado"])) {
    header("Location: loginProfe");
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crear tarea</title>
    <link rel="stylesheet" type="text/css" href="css/crear_usuario.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <script src="js/crear_tarea.js">
    



    </script>
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="/crear_usuario">Crear Usuario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link active" href="/crear_tarea">Crear Tarea</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/select">Listado Usuarios</a>
      </li>     
    </ul>
   
  </div>
</nav>


<div id="titulo"><h1>Crear tarea</h1></div>
    <form action="<?php echo $_SERVER["PHP_SELF"] ?>" method="post" enctype="multipart/form-data">
    
    <div class="contenedor" id="cont">
        <label>Introduce nombre de tarea:</label><br />
        <input type="text" name="nombre" /><br /><br />
        <label>Introduce descripción:</label><br />
        <input type="text" name="descripcion" /><br /><br />
        <label>Selecciona una imagen:</label><br />
        <input type="file" name="imagen" id="imagen"><br /><br />
        <label>Selecciona una locución:</label><br />
        <input type="file" name="audio" id="audio"><br /><br />
        <input type="submit" value="Enviar" name="submit">
    </div>
    </form>
    <div class="resultados">
    <?php
  
    if (isset($_POST["submit"])) {
        $imagenOK=false;
        $audioOK=false;
        if (trim($_POST["nombre"]) == "") {
            echo "Introduce un nombre";
        } else if (trim($_POST["descripcion"]) == "") {
            echo "Introduce una descripcion";
        } else {

            $target_dir = "imagenes/tareas/";
            $target_file = $target_dir . basename($_FILES["imagen"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            if ($_FILES["imagen"]["tmp_name"] != "") {
                $check = getimagesize($_FILES["imagen"]["tmp_name"]);
                if ($check == false) {
                    echo "<p>El fichero no es una imagen</p>";
                    $uploadOk = 0;
                } else {
                    //   echo "<p>File is an image - " . $check["mime"] . ".</p>";
                    $uploadOk = 1;


                    // Check if file already exists
                    if (file_exists($target_file)) {
                        echo "<p>ya existe una imagen con ese nombre...</p>";
                        $uploadOk = 0;
                    }
                    // Check file size
                    if ($_FILES["imagen"]["size"] > 500000) {
                        echo "<p>Archivo demasiado grande</p>";
                        $uploadOk = 0;
                    }
                    // Allow certain file formats
                    if (
                        $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                        && $imageFileType != "gif" 
                    ) {
                        echo "<p>Solo se permiten JPG, JPEG, PNG y GIF</p>";
                        $uploadOk = 0;
                    }
                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        echo "<p>Error al subir el fichero</p>";
                        // if everything is ok, try to upload file
                    } else {
                        if (move_uploaded_file($_FILES["imagen"]["tmp_name"], $target_file)) {
                            $imagenOK=true;

                            $uploadOk = 1;


                            $target_dir = "sonidos/";
                            $target_file = $target_dir . basename($_FILES["audio"]["name"]);
                            $uploadOk = 1;
                            $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
                            // Check if image file is a actual image or fake image
                            if ($_FILES["audio"]["tmp_name"] != "") {
                              
                
                                    // Check if file already exists
                                    if (file_exists($target_file)) {
                                        echo "<p>ya existe un audio con ese nombre...</p>";
                                        $uploadOk = 0;
                                    }
                                    // Check file size
                                    if ($_FILES["audio"]["size"] > 500000) {
                                        echo "<p>Archivo demasiado grande</p>";
                                        $uploadOk = 0;
                                    }
                                    // Allow certain file formats
                                    if (
                                        $imageFileType != "mp3" && $imageFileType != "wma" && $imageFileType != "ogg"
                                       
                                    ) {
                                        echo "<p>Solo se permiten MP3, WMA, y OGG</p>";
                                        $uploadOk = 0;
                                    }
                                    // Check if $uploadOk is set to 0 by an error
                                    if ($uploadOk == 0) {
                                        echo "<p>Error al subir el fichero</p>";
                                        // if everything is ok, try to upload file
                                    } else {
                                        if (move_uploaded_file($_FILES["audio"]["tmp_name"], $target_file)) {
                                            $audioOK=true;
                                        } else {
                                            echo "<p>Error al subir el fichero</p>";
                                        }
                                
                                }
                            } else {
                                echo "<p>Selecciona una audio</p>";
                            }
                        } else {
                            echo "<p>Error al subir el fichero</p>";
                        }
                    }
                }
            } else {
                echo "<p>Selecciona una imagen</p>";
            }
   


            if($imagenOK && $audioOK){
                
              
         
                $imagen = $_FILES["imagen"]["name"];
                $audio = $_FILES["audio"]["name"];
                $nombre = $_POST["nombre"];
                $descripcion = $_POST["descripcion"];
                echo "<script> var datos = {
                \"descripcion\":\"$descripcion\",
                \"locucion\":\"$audio\",
                \"imagen\":\"$imagen\",
                \"nombre\":\"$nombre\"
             
            };
            crear_tarea(datos);
            </script>";
            }
        }
    }
    ?>
    </div>
    <p id="res"></p>
</body>

</html>