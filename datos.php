<?php 
session_start();
if (!isset($_SESSION["usuario"]) || !($_SESSION["logueado"])) {
    header("Location: loginProfe");
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tareas pendientes</title>
    <link rel="stylesheet" href="/css/datos.css"/>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <script src="/js/datos.js"></script>

</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="/crear_usuario">Crear Usuario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/crear_tarea">Crear Tarea</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/select">Listado Usuarios</a>
      </li>     
    </ul>
   
  </div>
</nav>
<div class="parrafo">
    <h1 id="persona" ></h1>
	</div>
	<div class="fecha">
	<label>Fecha: </label>
    <input type="date" name="fecha" id="fecha" />
	</div>
	<br>
	<br>
	<div class="tbl">
    <table id="tabla"></table>
	</div>
	<br>
	<br>
    <form action="/select.php" method="post">
<div class="boton">
<input class="btnAtras" type="submit" value="Volver" ></input>
</div>
</form>
</body>

</html>