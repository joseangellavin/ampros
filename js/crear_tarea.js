function crear_tarea(datos){
    console.log("Enviando ...");
    fetch("https://api.ampros.es/tarea/crear", {
            method: "post",
            body: JSON.stringify(datos)
        }).then(r => r.json().then(data => ({ status: r.status, body: data })))
        .then(function(obj) {

            if (obj.body["exito"]) {
                console.log("Exito al crear");
                var p= document.getElementById("res");
                p.innerText="Tarea creada!";

            } else {
                console.log("Error al crear la tarea")
               var p= document.getElementById("res");
               p.innerText="Error al crear la tarea!";
                
            }
        }); 
}