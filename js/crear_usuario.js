function crear_usuario(datos){
    console.log("Enviando ...");
    fetch("https://api.ampros.es/usuario/crear", {
            method: "post",
            body: JSON.stringify(datos)
        }).then(r => r.json().then(data => ({ status: r.status, body: data })))
        .then(function(obj) {

            if (obj.body["exito"]) {
                console.log("Exito al crear");
                var p= document.getElementById("res");
                p.innerText="usuario creado!";

            } else {
                console.log("Error al crear el usuario")
               var p= document.getElementById("res");
               p.innerText="Error al crear usuario!";
                
            }
        }); 
}