function verTareas() {
    var url = window.location.href.split("/")

    var usuario = url[url.length - 1];
    var xhttp2 = new XMLHttpRequest();
    xhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:
            var datos = JSON.parse(xhttp2.responseText);
            for (d of datos) {
                document.getElementById("persona").innerText = "Tareas pendientes de " + d.nombre + " " + d.apellidos;
            }
        }
    };
    xhttp2.open("GET", "https://api.ampros.es/usuario/1", true);
    xhttp2.send();


    if (document.getElementById("fecha").value != "") {
        var fecha = document.getElementById("fecha").value;
    } else {
        var fecha = new Date().toLocaleDateString().split("/").reverse().join("-");
        var fecha = document.getElementById("fecha").value = fecha;


    }

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            // Typical action to be performed when the document is ready:

            var tareas = JSON.parse(xhttp.responseText);
            var tabla = document.getElementById("tabla");
            tabla.innerHTML = "<tr><th>Tareas pendientes:</th></tr>";
            for (t of tareas) {
                tabla.innerHTML += "<tr><td>" + t.descripcion + "</td></tr>"
            }
        }
    };
    xhttp.open("GET", "https://api.ampros.es/tareas/" + usuario + "/" + fecha, true);
    xhttp.send();

}
window.onload = function() {
    verTareas();
    document.getElementById("fecha").addEventListener("change", verTareas, false);
    var fecha = new Date().toLocaleDateString().split("/").reverse().join("-");
    document.getElementById("fecha").max = fecha;
}