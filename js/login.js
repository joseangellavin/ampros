var audioActivo = false;

window.onload = function() {
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var f = new Date();

    document.getElementById("fecha").innerText = (f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
    document.getElementById("hora").innerText = (f.toLocaleTimeString());

    setInterval(function() {
        document.getElementById("hora").innerText = (new Date().toLocaleTimeString());

    }, 1000);

}
function activarAudio() {

    if (audioActivo) {
        audioActivo = false;
        document.getElementById("toggleAudio").innerText = "Activar Audio";
    } else {
        audioActivo = true;
        document.getElementById("toggleAudio").innerText = "Desactivar Audio";

    }

}

function reproducirAudio() {
    if (audioActivo)
        document.getElementById(this.name).play();

}
//Mostrar datos
function login() {
    sessionStorage.setItem("id_usuario", this.name);
    window.location.href = "https://www.ampros.es/principal";
}

function loginProfe() {
    window.location.href = "https://www.ampros.es/loginProfe";

}



var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        // Typical action to be performed when the document is ready:

        var tareas = JSON.parse(xhttp.responseText);
        console.log(tareas);

        // Crear boton de login de profesor
        var botonProfe = document.createElement("button");
        botonProfe.className += "botonProfe";
        botonProfe.innerText = "Inicio de sesión Profesor|Tutor";
        botonProfe.addEventListener("click", loginProfe, false);
        document.getElementById("botonProfe").appendChild(botonProfe);


        for (t of tareas) {
            //crear el primer div
            var div1 = document.createElement("div");
            div1.classList.add("col-md-4");
            //crear div2 y meterlo en div1 
            var div2 = document.createElement("div");
            div2.className += "card mb-4 shadow-sm";
            div2.id = t.id;

            //crear imagen y meterla en div2
            var img = document.createElement("img");
            img.src = "imagenes/usuarios/" + t.imagen_login;
            img.classList+="centrada"
            img.title = t.usuario;
            img.alt = t.usuario;
            img.width = 250;
            img.name = t.id;
            // img.addEventListener("mouseover", reproducirAudio, false);
            img.addEventListener("click", login, false);
            //  var audio = document.createElement("audio");
            //  audio.id = t.nombre;
            //  var source = document.createElement("source");
            // source.src = t.locucion;
            // source.type = "audio/mpeg";
            //audio.appendChild(source);
            // div2.appendChild(audio);
            div2.appendChild(img);
            //crear div3 y meterlo en div2
            var div3 = document.createElement("div");
            div3.classList.add("card-body");


            //crear div4 y meterlo en div3
            var div4 = document.createElement("div");
            div4.className += "d-flex justify-content-between align-items-center";


            //crear div5 y meterlo en div4
            var div5 = document.createElement("div");
            div5.classList.add("btn-group");

            //crear botones y meterlos en div5
            var botonEntrar = document.createElement("button");
            botonEntrar.classList.add("myButton");
            //      botonEntrar.id = t.id;
            botonEntrar.addEventListener("click", login, false);

            botonEntrar.name = t.id;
            botonEntrar.innerText = t.nombre;
            botonEntrar.innerText += " " + t.apellidos;
            //var audio = document.createElement("audio");
            //audio.id = this.id;
            //var source = document.createElement("source");
            //source.src = "sonidos/si.mp3";
            //source.type = "audio/mpeg";
            //audio.appendChild(source);
            //div5.appendChild(audio);
            botonEntrar.addEventListener("mouseover", reproducirAudio, false);
            div5.appendChild(botonEntrar);




            div4.appendChild(div5);
            div3.appendChild(div4);

            div2.appendChild(div3);
            div1.appendChild(div2);




            document.getElementById("contenido").appendChild(div1);
        }
    }
}

xhttp.open("GET", "https://api.ampros.es/login/imagenes", true);
xhttp.send();