 //Mostrar datos

 if (sessionStorage.getItem("id_usuario") == "" || sessionStorage.getItem("id_usuario") == null) {
     window.location.href = "https://www.ampros.es/login";
 }

 var audioActivo = false;

 window.onload = function() {
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    var f = new Date();

    document.getElementById("fecha").innerText = (f.getDate() + " de " + meses[f.getMonth()] + " de " + f.getFullYear());
    document.getElementById("hora").innerText = (f.toLocaleTimeString());

    setInterval(function() {
        document.getElementById("hora").innerText = (new Date().toLocaleTimeString());

    }, 1000);
    document.getElementById("toggleAudio").addEventListener("click",activarAudio,false);
}
 function activarAudio() {

     if (audioActivo) {
         audioActivo = false;
         document.getElementById("toggleAudio").innerHTML = "<img src='imagenes/active.svg' class='altavoz'>";
     } else {
         audioActivo = true;
         document.getElementById("toggleAudio").innerHTML = "<img src='imagenes/mute.svg' class='altavoz'>";





     }

 }

 function completarTarea() {
     var id = this.id;
     var datos = {
         "id_usuario": sessionStorage.getItem("id_usuario"),
         "hora": new Date().toLocaleTimeString(),
         "fecha": new Date().toLocaleDateString().split("/").reverse().join("-"),
         "id_tarea": id
     }
     console.log('Enviando ...');
     fetch('https://api.ampros.es/tarea/completar', {
             method: 'post',
             body: JSON.stringify(datos)
         }).then(r => r.json().then(data => ({ status: r.status, body: data })))
         .then(function(obj) {

             if (obj.body["exito"]) {
                 console.log("Exito al completar");
                 document.getElementById(id).style.display = "none";

             } else {
                 console.log("Error al completar la tarea")
             }
         });
     // document.getElementById(this.id).style.display = "none";
 }

 function reproducirAudio() {
     if (audioActivo)
         document.getElementById(this.name).play();

 }


 var xhttp = new XMLHttpRequest();

 xhttp.onreadystatechange = function() {
     if (this.readyState == 4 && this.status == 200) {
         // Typical action to be performed when the document is ready:
      


       
         var tareas = JSON.parse(xhttp.responseText);
         for (t of tareas) {
             //crear el primer div
             var div1 = document.createElement("div");
             div1.classList.add("col-md-4");
             //crear div2 y meterlo en div1 
             var div2 = document.createElement("div");
             div2.className += "card mb-4 shadow-sm";
             div2.id = t.id;

             //crear imagen y meterla en div2

             var img = document.createElement("img");
             img.src = "imagenes/tareas/" + t.imagen;
             img.title = t.descripcion;
             img.alt = t.descripcion;
             img.width = 300;
             img.className+=" imagenTarea"
             img.name = t.nombre;
             img.addEventListener("mouseover", reproducirAudio, false);

             var audio = document.createElement("audio");
             audio.id = t.nombre;
             var source = document.createElement("source");
             source.src = "sonidos/" + t.locucion;
             source.type = "audio/mpeg";
             audio.appendChild(source);
             div2.appendChild(audio);
             div2.appendChild(img);
             //crear div3 y meterlo en div2
             var div3 = document.createElement("div");
             div3.classList.add("card-body");

             //crear parrafo y meterlo en div3
             var parrafo = document.createElement("p");
             parrafo.classList.add("card-text");
             parrafo.innerText = t.descripcion;
             div3.appendChild(parrafo);
             //crear div4 y meterlo en div3
             var div4 = document.createElement("div");
             div4.className += "d-flex justify-content-between align-items-center";


             //crear div5 y meterlo en div4
             var div5 = document.createElement("div");
             div5.classList.add("btn-group");

             //crear botones y meterlos en div5
             var botonSi = document.createElement("button");
             var botonSiIMG = document.createElement("img");
             botonSi.classList.add("myButton");
             botonSi.id = t.id;
             botonSi.addEventListener("click", completarTarea, false);
             botonSi.href = "#";
             botonSi.name = "Si"
             botonSi.innerText = "Si";
             botonSiIMG.src = "imagenes/imagenes_botones/si.png";
             botonSiIMG.width = 50;
             botonSi.appendChild(botonSiIMG);

             var audio = document.createElement("audio");
             audio.id = "Si"
             var source = document.createElement("source");
             source.src = "sonidos/si.mp3";
             source.type = "audio/mpeg";
             audio.appendChild(source);
             div5.appendChild(audio);
             botonSi.addEventListener("mouseover", reproducirAudio, false);




             div5.appendChild(botonSi);
           /*  var botonNo = document.createElement("button");
             var botonNoIMG = document.createElement("img");
             botonNo.classList.add("myButton1");
             botonNo.href = "#";
             botonNo.innerText = "No";
             botonNo.name = "No"
             botonNoIMG.src = "imagenes/imagenes_botones/no.png";
             botonNoIMG.width = 50;
             botonNo.appendChild(botonNoIMG);
             var audio = document.createElement("audio");
             audio.id = "No";
             var source = document.createElement("source");
             source.src = "sonidos/no.mp3";
             source.type = "audio/mpeg";
             audio.appendChild(source);
             botonNo.addEventListener("mouseover", reproducirAudio, false);

             div5.appendChild(botonNo);
             div5.appendChild(audio);
*/
             div4.appendChild(div5);
             div3.appendChild(div4);

             div2.appendChild(div3);
             div1.appendChild(div2);




             document.getElementById("contenido").appendChild(div1);
         }
     }
 };
 xhttp.open("GET", "https://api.ampros.es/tareas/" + sessionStorage.getItem("id_usuario"), true);
 xhttp.send();


 