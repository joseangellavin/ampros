  //Mostrar datos
  function verDatos() {
    window.location.href= "https://www.ampros.es/ver/"+this.id;
}
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:

       var tareas = JSON.parse(xhttp.responseText);
       console.log(tareas);
        for (t of tareas) {
            //crear el primer div
            var div1 = document.createElement("div");
            div1.classList.add("col-lg-4");
            
            //crear imagen, titulo y boton y meterla en div2
            var img = document.createElement("img");
            img.src="imagenes/usuarios/" +t.imagen_login;
            img.className+= "bd-placheholder-img rounded-circle";
            img.title = t.usuario;
            img.alt = t.usuario;
            img.width = 100;
            img.name = t.id;

            var h2 = document.createElement("h2");
            h2.innerText= t.nombre;
            h2.innerText+= " "+t.apellidos;

            var parrafo = document.createElement("p");
            var a = document.createElement("button");
            a.className+="btn btn-secondary";
            a.href= "#";
            a.id=t.id;
            a.innerHTML= "Ver Rutina &raquo;"
            a.addEventListener("click", verDatos, false);
            parrafo.appendChild(a);

            div1.appendChild(img);
            div1.appendChild(h2);
            div1.appendChild(parrafo);




            document.getElementById("contenido").appendChild(div1);
        }
    }
};
xhttp.open("GET", "https://api.ampros.es/usuarios/"+sessionStorage.getItem("id_tutor"), true);
xhttp.send();