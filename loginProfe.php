


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">

    <title>Login Tutores</title>



    <!-- Bootstrap core CSS -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/loginProfe.css">

    <!-- Favicons -->



  
    <!-- Custom styles for this template -->
    
  </head>
  <body class="text-center">
  <div class="foto">
  <img src="imagenes/ampros.png">
  </div>
  <div class="container" id="log">
    <form class="form-signin" method="post" action="<?php echo $_SERVER["PHP_SELF"] ?>">
  
  <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesion</h1>
  <label for="inputEmail" class="sr-only">Usuario</label>
  <input name="usuario" type="text" id="inputEmail" class="form-control" placeholder="Usuario" required autofocus>
  <br>
  <label for="inputPassword" class="sr-only">Contraseña</label>
  <input  name="contrasenia" type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
  <div class="checkbox mb-3">
    
  </div>
  <input name="login" class="btn btn-lg btn-primary btn-block" type="submit" value="Iniciar sesion"/>
  <?php

if(isset($_POST["login"])){
  $usuario=$_POST["usuario"];
  $pass=$_POST["contrasenia"];
  require "api/db.php";
    //print_r($tarea);
    $sql = "SELECT * FROM tutor WHERE usuario LIKE ? AND contrasenia LIKE '" . md5($pass) . "';";

    try {
        $conexion = getConnection();
        $sentencia = $conexion->prepare($sql);
        $sentencia->bind_param("s", $usuario);
        if (!$sentencia->execute()) {
            echo "Falló la ejecución 1: (" . $sentencia->errno . ") " . $sentencia->error;
        } else {
            $resultado = $sentencia->get_result();
            $arrayResultados = $resultado->fetch_all(MYSQLI_ASSOC);
            if (sizeof($arrayResultados) == 0) {
                echo '  <p class="mt-5 mb-3">Usuario o contraseña incorrectos!</p>';
            } else {
              session_start();
                $id=$arrayResultados[0]['id'];
              $_SESSION["id_usuario"]=$id;
              echo "<script>sessionStorage.setItem('id_tutor','$id')</script>";
              $_SESSION["usuario"]=$usuario;
              $_SESSION["logueado"]=true;
               header("Refresh:0.5, url=/select");
            }
           
        }
        $sentencia->close();
        $conexion->close();
    } catch (Exception $e) {

        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

?>
<br/>
 <a href="/" class="btn btn-lg btn-primary btn-block">Volver a Principal</a>
  <p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>

  
</form>
</div>
</body>
</html>