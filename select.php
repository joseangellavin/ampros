<?php

session_start();
if (!isset($_SESSION["usuario"]) || !($_SESSION["logueado"])) {
  header("Location: loginProfe.php");
}
if (isset($_POST['btn'])) {
  session_destroy();
  echo "<script>sessionStorage.setItem('id_tutor','')</script>";

  header("Refresh:0.5, url=/loginProfe");
}


?>

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Rutinas AMPROS</title>

 

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/select.css">



  <script src="js/select.js">
  </script>


</head>

<body>

  <main role="main">


    <header>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="/crear_usuario">Crear Usuario</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/crear_tarea">Crear Tarea</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Listado Usuarios</a>
      </li>
    </ul>
   
  </div>
</nav>
      <div class="cabezera">
        <h1>Selecciona un perfil</h1>
        <br>
        <form action="select.php" method="post">
          <div class="boton">
            <input class="btnAtras" type="submit" value="Cerrar Sesión" name="btn"></input>
          </div>
        </form>
      </div>
    </header>



    <div class="container">


      <div class="row" id="contenido">



      </div>





  </main>

</html>