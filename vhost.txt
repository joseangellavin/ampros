<VirtualHost *:443>
    ##ServerAdmin webmaster@dummy-host2.example.com
    DocumentRoot "D:/xampp/htdocs/ampros/api"
   ServerName api.ampros.es
   SSLEngine On
   SSLCertificateFile "D:\xampp\certificados\ampros.crt"
   SSLCertificatekeyFile "D:\xampp\certificados\ampros.key"

</VirtualHost>
<VirtualHost *:80>
    ##ServerAdmin webmaster@dummy-host2.example.com
    DocumentRoot "D:/xampp/htdocs/ampros"
   ServerName www.ampros.es
   Redirect permanent / https://www.ampros.es

</VirtualHost>
<VirtualHost *:443>
    ##ServerAdmin webmaster@dummy-host2.example.com
    DocumentRoot "D:/xampp/htdocs/ampros"
   ServerName www.ampros.es
   SSLEngine On
   SSLCertificateFile "D:\xampp\certificados\ampros.crt"
   SSLCertificatekeyFile "D:\xampp\certificados\ampros.key"

</VirtualHost>